# food_truck_tracker

An application to keep track of Food trucks in your neighbour.

## Getting Started

Its a flutter based application. In order to use this application, simply follow below steps:

1. By default there would be no food trucks added and therefore there would be no markers on the google maps.
In order to add Food trucks, simply click on the location of food truck on the map and the add button will turn blue.

2. Click on blue add floating action button, a popup would appear where you can add all other details of the truck.

3. In order to edit the Food truck details, simply click on marker on the map and an edit pop up will appear.

4. Using the filter floating action button, you can filter out the food trucks by their categories.

