import 'package:flutter/material.dart';
import 'package:food_truck_tracker/models/FoodTruck.dart';

class AppUtil {
  static Map<FoodTruckType, String> typeToString = {
    FoodTruckType.CHAAT: 'Chaat',
    FoodTruckType.CHINESE: 'Chinese',
    FoodTruckType.INDIAN: 'Indian',
    FoodTruckType.ITALIAN: 'Italian',
    FoodTruckType.PATTIES_BURGER: 'Patties and Burger',
    FoodTruckType.SANDWITCHES: 'SandWitches',
  };

  static showLoadingDailog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text('Please wait'),
          children: <Widget>[
            Center(
              child: CircularProgressIndicator(),
            ),
          ],
        );
      },
    );
  }

  static double timeToDouble(TimeOfDay myTime) {
    if (myTime.period == DayPeriod.pm) {
      return myTime.hour + 12 + myTime.minute / 60.0;
    }
    return myTime.hour + myTime.minute / 60.0;
  }

  static TimeOfDay stringToTimeOfDay(String time) {
    String amPM = time.split(':').elementAt(1).split(' ').elementAt(1);
    int hour = int.parse(time.split(':').first);
    TimeOfDay t = TimeOfDay(
        hour: amPM == 'PM' ? hour + 12 : hour,
        minute: int.parse(time.split(':').elementAt(1).split(' ').first));

    return t;
  }
}
