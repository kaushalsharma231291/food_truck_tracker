import 'package:food_truck_tracker/models/FoodTruck.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class DatabaseHelper {
  static Future<sql.Database> _getSqlDatabase() async {
    final databasePath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(databasePath, 'foodTruckDatabase.db'),
        onCreate: (database, version) async {
      await database.execute(
          "CREATE TABLE FoodTruck(id INT PRIMARY KEY, name TEXT, address TEXT, type TEXT, openingTime TEXT, closingTime TEXT, latitude REAL, longitude REAL)");
    }, version: 1);
  }

  static Future<void> insert(String tableName, FoodTruck foodTruck) async {
    final sqlDatabase = await _getSqlDatabase();
    await sqlDatabase.insert(
      tableName,
      foodTruck.toJson(),
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<FoodTruck>> getAllFoodTrucks() async {
    final sqlDatabase = await _getSqlDatabase();
    List<FoodTruck> foodTruckList = [];
    List<Map<String, Object>> tableEntries =
        await sqlDatabase.query('FoodTruck');
    tableEntries.forEach((entry) {
      FoodTruck foodTruck = FoodTruck.fromJson(entry);
      foodTruckList.add(foodTruck);
    });
    return foodTruckList;
  }
}
