import 'package:flutter/material.dart';
import 'package:food_truck_tracker/models/FoodTruck.dart';
import 'package:food_truck_tracker/util/DatabaseHelper.dart';
import 'package:food_truck_tracker/HomeScreen.dart';
import 'package:food_truck_tracker/util/AppUtil.dart';

class AddEditFoodTruckDailog extends StatefulWidget {
  final FoodTruck foodTruck;

  AddEditFoodTruckDailog({this.foodTruck});

  @override
  _AddEditFoodTruckDailogState createState() => _AddEditFoodTruckDailogState();
}

class _AddEditFoodTruckDailogState extends State<AddEditFoodTruckDailog> {
  final _addEditformKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  FoodTruckType _selectedType;

  TextEditingController _nameController;

  TextEditingController _addressController;

  String openingTime;

  String closingTime;

  List<DropdownMenuItem<FoodTruckType>> _typeList;

  @override
  void initState() {
    _nameController = TextEditingController(text: widget.foodTruck.name);
    _addressController = TextEditingController(text: widget.foodTruck.address);
    _selectedType = widget.foodTruck.type;
    openingTime = widget.foodTruck.openingTime;
    closingTime = widget.foodTruck.closingTime;
    _typeList = [
      DropdownMenuItem(
        value: FoodTruckType.CHAAT,
        child: Text('Chaat'),
      ),
      DropdownMenuItem(
        value: FoodTruckType.CHINESE,
        child: Text('Chinese'),
      ),
      DropdownMenuItem(
        value: FoodTruckType.INDIAN,
        child: Text('Indian'),
      ),
      DropdownMenuItem(
        value: FoodTruckType.ITALIAN,
        child: Text('Italian'),
      ),
      DropdownMenuItem(
        value: FoodTruckType.PATTIES_BURGER,
        child: Text('Patties and Burger'),
      ),
      DropdownMenuItem(
        value: FoodTruckType.SANDWITCHES,
        child: Text('Sandwitches'),
      )
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text(widget.foodTruck.name != null
          ? 'Edit Food Truck Details'
          : 'Add Food Truck Details'),
      children: <Widget>[
        Container(
          child: SingleChildScrollView(
            child: Form(
              key: _addEditformKey,
              autovalidate: _autoValidate,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(
                          labelText: 'Name',
                          hintText: 'Ex : Delicious Raj Bhojan'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Required';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: TextFormField(
                      controller: _addressController,
                      decoration: InputDecoration(
                          labelText: 'Address',
                          hintText: 'Ex : Block B, Connaught Place'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Required';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: DropdownButtonFormField<FoodTruckType>(
                        value: _selectedType,
                        decoration:
                            InputDecoration(labelText: 'Food Truck Type'),
                        validator: (value) {
                          if (value == null) {
                            return 'Required';
                          }
                        },
                        items: _typeList,
                        onChanged: (type) {
                          _selectedType = type;
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: InkWell(
                      onTap: () {
                        showTimePicker(
                          initialTime: TimeOfDay.now(),
                          context: context,
                        ).then((openingTimeValue) {
                          if (openingTimeValue != null) {
                            setState(() {
                              openingTime = openingTimeValue.format(context);
                            });
                          }
                        });
                      },
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        child: openingTime == null
                            ? Center(
                                child: Text(
                                'Please click here and choose opening time',
                                style: TextStyle(color: Colors.red),
                              ))
                            : Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text('Opening Time'),
                                  ),
                                  Expanded(
                                    child: SizedBox(),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Text(openingTime),
                                  )
                                ],
                              ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16.0),
                    child: InkWell(
                      onTap: () {
                        showTimePicker(
                          initialTime: TimeOfDay.now(),
                          context: context,
                        ).then((closingTimeValue) {
                          if (closingTimeValue != null) {
                            setState(() {
                              closingTime = closingTimeValue.format(context);
                            });
                          }
                        });
                      },
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        child: closingTime == null
                            ? Center(
                                child: Text(
                                'Please click here and choose closing time',
                                style: TextStyle(color: Colors.red),
                              ))
                            : Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text('Closing Time'),
                                  ),
                                  Expanded(
                                    child: SizedBox(),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Text(closingTime),
                                  )
                                ],
                              ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      color: Colors.blue,
                      onPressed: () async {
                        if (_addEditformKey.currentState.validate() &&
                            openingTime != null &&
                            closingTime != null) {
                          AppUtil.showLoadingDailog(context);
                          FoodTruck ft = FoodTruck(
                            id: widget.foodTruck.id,
                            name: _nameController.text,
                            address: _addressController.text,
                            closingTime: closingTime,
                            openingTime: openingTime,
                            type: _selectedType,
                            latitude: widget.foodTruck.latitude,
                            longitude: widget.foodTruck.longitude,
                          );
                          await DatabaseHelper.insert('FoodTruck', ft);
                          var route = MaterialPageRoute(
                            builder: (context) =>
                                HomeScreen(title: 'Food Truck Tracker'),
                          );

                          Navigator.pushAndRemoveUntil(
                              context, route, (r) => false);
                        } else {
                          setState(() {
                            _autoValidate = true;
                          });
                        }
                      },
                      child:
                          Text('Submit', style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
