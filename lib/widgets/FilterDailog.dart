import 'package:flutter/material.dart';
import 'package:food_truck_tracker/models/FoodTruck.dart';
import 'package:food_truck_tracker/util/AppUtil.dart';

class FilterDailog extends StatefulWidget {
  List<FoodTruckType> selectedTypes;
  Function onSubmitPressed;

  FilterDailog({this.selectedTypes, this.onSubmitPressed});

  @override
  _FilterDailogState createState() => _FilterDailogState();
}

class _FilterDailogState extends State<FilterDailog> {
  List<FoodTruckType> _choosenTypes = [];
  List<Widget> items = [];

  @override
  void initState() {
    _choosenTypes.addAll([...widget.selectedTypes]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    items.clear();
    FoodTruckType.values.forEach((type) {
      items.add(ListTile(
        leading: Icon(_choosenTypes.contains(type)
            ? Icons.check_box
            : Icons.check_box_outline_blank),
        title: Text(AppUtil.typeToString[type]),
        onTap: () {
          setState(() {
            bool foundElement = _choosenTypes.remove(type);

            if (!foundElement) {
              _choosenTypes.add(type);
            }
          });
        },
      ));
    });

    return SimpleDialog(
      title: Text('Filter By Type'),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          child: Container(
              child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: items,
            ),
          )),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
            color: Colors.blue,
            onPressed: () {
              widget.onSubmitPressed(_choosenTypes);
              Navigator.of(context).pop();
            },
            child: Text('Submit', style: TextStyle(color: Colors.white)),
          ),
        ),
      ],
    );
  }
}
