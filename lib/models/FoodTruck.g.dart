// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FoodTruck.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FoodTruck _$FoodTruckFromJson(Map<String, dynamic> json) {
  return FoodTruck(
    id: json['id'] as int,
    name: json['name'] as String,
    type: _$enumDecodeNullable(_$FoodTruckTypeEnumMap, json['type']),
    address: json['address'] as String,
    latitude: (json['latitude'] as num)?.toDouble(),
    longitude: (json['longitude'] as num)?.toDouble(),
    openingTime: json['openingTime'] as String,
    closingTime: json['closingTime'] as String,
  );
}

Map<String, dynamic> _$FoodTruckToJson(FoodTruck instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': _$FoodTruckTypeEnumMap[instance.type],
      'address': instance.address,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'openingTime': instance.openingTime,
      'closingTime': instance.closingTime,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$FoodTruckTypeEnumMap = {
  FoodTruckType.CHINESE: 'CHINESE',
  FoodTruckType.SANDWITCHES: 'SANDWITCHES',
  FoodTruckType.PATTIES_BURGER: 'PATTIES_BURGER',
  FoodTruckType.CHAAT: 'CHAAT',
  FoodTruckType.INDIAN: 'INDIAN',
  FoodTruckType.ITALIAN: 'ITALIAN',
};
