import 'package:json_annotation/json_annotation.dart';

part 'FoodTruck.g.dart';

enum FoodTruckType {
  CHINESE,
  SANDWITCHES,
  PATTIES_BURGER,
  CHAAT,
  INDIAN,
  ITALIAN,
}

@JsonSerializable()
class FoodTruck {
  final int id;
  final String name;
  final FoodTruckType type;
  final String address;
  final double latitude;
  final double longitude;
  final String openingTime;
  final String closingTime;

  FoodTruck({
    this.id,
    this.name,
    this.type,
    this.address,
    this.latitude,
    this.longitude,
    this.openingTime,
    this.closingTime,
  });

  factory FoodTruck.fromJson(Map<String, dynamic> json) =>
      _$FoodTruckFromJson(json);

  Map<String, dynamic> toJson() => _$FoodTruckToJson(this);
}
