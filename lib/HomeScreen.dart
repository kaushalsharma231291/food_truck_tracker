import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:food_truck_tracker/models/FoodTruck.dart';
import 'package:food_truck_tracker/util/DatabaseHelper.dart';
import 'package:food_truck_tracker/widgets/AddEditFoodTruckDailog.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';
import 'package:food_truck_tracker/widgets/FilterDailog.dart';
import 'package:food_truck_tracker/util/AppUtil.dart';

class HomeScreen extends StatefulWidget {
  final String title;

  HomeScreen({this.title});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Completer<GoogleMapController> _completer = Completer();
  GoogleMapController _mapController;
  Set<Marker> truckLocationMarkers = Set();
  Future<List<FoodTruck>> truckData;
  Marker _newMarker;
  List<FoodTruckType> selectedTypes = [];
  List<FoodTruck> allTrucks = [];
  bool _updateMarkers = true;

  static final CameraPosition _initialCameraPosition = CameraPosition(
    target: LatLng(28.6304, 77.2177),
    zoom: 14,
  );

  @override
  void initState() {
    truckData = DatabaseHelper.getAllFoodTrucks();
    super.initState();
  }

  _showAllInfoWindows() {
    if (truckLocationMarkers.length > 0) {
      truckLocationMarkers.forEach((marker) {
        if (marker.infoWindow != null) {
          _mapController.showMarkerInfoWindow(marker.markerId);
        }
      });
    }
  }

  _onMapTap(LatLng position) {
    if (_newMarker == null) {
      setState(() {
        _newMarker = Marker(
          markerId: MarkerId(Random().nextInt(1000).toString()),
          position: position,
          draggable: true,
          onDragEnd: (newPosition) {},
          icon: BitmapDescriptor.defaultMarkerWithHue(
              BitmapDescriptor.hueMagenta),
        );
        truckLocationMarkers.add(_newMarker);
      });
      _showAllInfoWindows();
    }
  }

  _showAddEditDailog(FoodTruck foodTruck) {
    showDialog(
        context: context,
        builder: (context) {
          return AddEditFoodTruckDailog(
            foodTruck: foodTruck,
          );
        });
  }

  _onFilterSubmit(List<FoodTruckType> choosenTypes) {
    truckLocationMarkers.clear();
    selectedTypes.clear();
    if (choosenTypes.length > 0) {
      _updateMarkers = false;
      selectedTypes.addAll([...choosenTypes]);
      setState(() {
        allTrucks.forEach((foodTruck) {
          if (selectedTypes.contains(foodTruck.type)) {
            TimeOfDay nowTime = TimeOfDay.now();
            TimeOfDay openTime =
                AppUtil.stringToTimeOfDay(foodTruck.openingTime);
            TimeOfDay closeTime =
                AppUtil.stringToTimeOfDay(foodTruck.closingTime);
            bool closed = false;

            if (AppUtil.timeToDouble(nowTime) >=
                AppUtil.timeToDouble(closeTime)) {
              closed = true;
            }

            if (AppUtil.timeToDouble(nowTime) <=
                AppUtil.timeToDouble(openTime)) {
              closed = true;
            }
            truckLocationMarkers.add(Marker(
              markerId: MarkerId(foodTruck.id.toString()),
              position: LatLng(foodTruck.latitude, foodTruck.longitude),
              icon: BitmapDescriptor.defaultMarkerWithHue(
                  closed ? BitmapDescriptor.hueRed : BitmapDescriptor.hueGreen),
              infoWindow:
                  InfoWindow(title: foodTruck.name, snippet: foodTruck.address),
              onTap: () {
                _showAddEditDailog(foodTruck);
              },
            ));
          }
        });
      });
    } else {
      setState(() {
        _updateMarkers = true;
      });
    }
  }

  _showFilterDailog() {
    showDialog(
        context: context,
        builder: (context) {
          return FilterDailog(
            selectedTypes: selectedTypes,
            onSubmitPressed: _onFilterSubmit,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    double _deviceHeight = MediaQuery.of(context).size.height;
    double _deviceWidth = MediaQuery.of(context).size.width;

    Widget _getMainContainer(List<FoodTruck> foodTruckList) {
      if (truckLocationMarkers.length == 0 && _updateMarkers) {
        foodTruckList.forEach((foodTruck) {
          TimeOfDay now = TimeOfDay.now();
          TimeOfDay nowTime = AppUtil.stringToTimeOfDay(now.format(context));
          TimeOfDay openTime = AppUtil.stringToTimeOfDay(foodTruck.openingTime);
          TimeOfDay closeTime =
              AppUtil.stringToTimeOfDay(foodTruck.closingTime);
          bool closed = false;
          if (AppUtil.timeToDouble(nowTime) > AppUtil.timeToDouble(closeTime)) {
            closed = true;
          }

          if (AppUtil.timeToDouble(nowTime) < AppUtil.timeToDouble(openTime)) {
            closed = true;
          }

          truckLocationMarkers.add(Marker(
            markerId: MarkerId(foodTruck.id.toString()),
            position: LatLng(foodTruck.latitude, foodTruck.longitude),
            icon: BitmapDescriptor.defaultMarkerWithHue(
                closed ? BitmapDescriptor.hueRed : BitmapDescriptor.hueGreen),
            infoWindow:
                InfoWindow(title: foodTruck.name, snippet: foodTruck.address),
            onTap: () {
              _showAddEditDailog(foodTruck);
            },
          ));
        });
      }

      return GoogleMap(
        mapType: MapType.normal,
        markers: truckLocationMarkers,
        initialCameraPosition: _initialCameraPosition,
        onMapCreated: (GoogleMapController controller) {
          _completer.complete(controller);
          _mapController = controller;
          _showAllInfoWindows();
        },
        zoomControlsEnabled: false,
        onTap: _onMapTap,
      );
    }

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: truckData,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Container(
                height: _deviceHeight * 0.80,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
              break;
            case ConnectionState.waiting:
              return Container(
                height: _deviceHeight * 0.80,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
              break;
            case ConnectionState.active:
              return Container(
                height: _deviceHeight * 0.80,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
              break;
            case ConnectionState.done:
              if (snapshot.hasData) {
                List<FoodTruck> foodTruckList = snapshot.data;
                allTrucks.clear();
                allTrucks.addAll([...foodTruckList]);
                return _getMainContainer(foodTruckList);
              } else {
                return Container(
                  height: _deviceHeight * 0.80,
                  child: Center(
                    child: Text('Something went wrong. Please try again.'),
                  ),
                );
              }
              break;
          }
        },
      ),
      floatingActionButton: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: FloatingActionButton.extended(
              onPressed: _showFilterDailog,
              label: Text('Filter'),
              icon: Icon(Icons.filter_list),
            ),
          ),
          FloatingActionButton.extended(
            backgroundColor:
                _newMarker != null ? Colors.blue : Colors.grey[600],
            onPressed: () {
              if (_newMarker != null) {
                LatLng position = _newMarker.position;
                FoodTruck newFoodTruck = FoodTruck(
                    id: Random().nextInt(1000),
                    latitude: position.latitude,
                    longitude: position.longitude);
                _showAddEditDailog(newFoodTruck);
              } else {
                Toast.show(
                    "Please tap on map to point at truck location.", context,
                    duration: 5);
              }
            },
            label: Text('Add more trucks'),
            icon: Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}
